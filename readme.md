# Exercises

### Create test cases
* Create at least 6 scenarios
    * Try to have at least 2 different feature files
* Try to keep your selectors to the point
    
### Add screenshots to your reporting
* Add a manual taken screenshot to your reporting

### Automatically add screenshots
* Use a selenium provided listener to automatically take screenshots

### Multiple browser
* Use a system property to define which browser
* Use a factory pattern
