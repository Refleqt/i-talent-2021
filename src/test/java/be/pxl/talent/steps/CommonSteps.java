package be.pxl.talent.steps;

import be.pxl.talent.pages.HomePage;
import be.pxl.talent.support.DriverProvider;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;

public class CommonSteps {

    @After
    public void after() {
        DriverProvider.destroyDriver();
    }

    @Given("I navigate to {string}")
    public void navigateTo(String url) {
        DriverProvider.navigate(url);
    }

    @Given("I accept the cookie")
    public void iAcceptTheCookie() {
        new HomePage().acceptCookie();
    }
}
