package be.pxl.talent.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends AbstractPage {

    @FindBy(name = "accept_cookie")
    private WebElement acceptCookieBtn;

    public HomePage acceptCookie() {
        waitForElement(acceptCookieBtn);
        acceptCookieBtn.click();
        return this;
    }
}
