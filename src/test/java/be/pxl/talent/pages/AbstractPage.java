package be.pxl.talent.pages;

import be.pxl.talent.support.DriverProvider;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {

    private final WebDriverWait wait = new WebDriverWait(DriverProvider.getDriver(), 10L);

    public AbstractPage() {
        PageFactory.initElements(DriverProvider.getDriver(), this);
    }

    public void waitForElement(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }
}
