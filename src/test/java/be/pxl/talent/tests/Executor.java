package be.pxl.talent.tests;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeSuite;

@CucumberOptions(
        glue = "be.pxl.talent.steps",
        features = "src/test/resources/features",
        plugin = {"html:target/cucumber/index.html"}
)
public class Executor extends AbstractTestNGCucumberTests {

    @BeforeSuite
    public void beforeSuite() {
        System.setProperty("cucumber.publish.quiet", "true");
    }
}
